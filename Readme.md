MSGarageDoorRemote
===========================

Un mando a distancia más seguro para la puerta del garaje. Dado que la seguridad del que tenía dejaba bastante de desear y se pudo abrir la puerta con un clon de Arduino y un transmisor a 433 Mhz (solamente 6 € en en hardware).

![Mando](https://3.bp.blogspot.com/-7FA9-XSJQvU/XFBjPEz8ohI/AAAAAAAABpY/QrmGIUekdf8G7fY1KaivIatr-wUhXWx5ACKgBGAs/s1600/Placa_mando.png)


Las especificaciones de este nuevo mando son las siguientes:

 - Funcionamiento a 868 Mhz en vez de los habituales 433 Mhz.
 - Modulación FSK en vez de OOK.
 - Encriptación segura por hardware con AES de 128 bits.
 - Hasta 128 claves de 32 bytes que van rotando.
 - Watchdog en la unidad del motor.
 - Potencia de transmisión programable. (Hasta 20 dBm. 5dBm por defecto).
 - Open source (se pude cambiar el protocolo o añadir funcionalidades si se quiere).
 - Se pude utilizar un mismo mando para abrir diferentes puertas.

Para más información consultar el [blog.](https://www.circuiteando.net/2019/01/msgaragedoorremote-parte-1-seguridad.html)