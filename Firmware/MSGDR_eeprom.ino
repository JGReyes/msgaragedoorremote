/*
 * File: MSGDR_eeprom.ino
 * Project: MSGarageDoorRemote
 * Created Date: 18/09/2018 8:58:42 pm
 * Author: JGReyes at <josegpuntoreyes@gmail.com>
 * -----
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any
 * later version.
 *
 *      This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 *      You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http: //www.gnu.org/licenses/>.
 * -----
 * Last Modified: 26/09/2018 7:14:32 pm
 * Modified By: JGReyes at josegpuntoreyes@gmail.com
 * -----
 * Copyright (c) 2018 JGReyes
 * -----
 * HISTORY:
 * Date      	By	JGReyes
 * ----------	---	---------------------------------------------------------
 */

// Versión para grabar las contraseñas en la EEPROM externa.
// Fusibles a la hora de grabar el micro. lfuse: 0xFF hfuse: 0xDF efuse: 0xFE

#include <Wire.h>      //incluido con Arduino IDE
#include <extEEPROM.h> //https://github.com/JChristensen/extEEPROM
#include <Flash.h>     //https://github.com/schinken/Flash

#define RELAYPIN 6 // Pin donde está conectado el relé.
#define LED 7
#define RFM69_RESET 4 // Pin de reset para poder deshabilitar el RFM69.

//#define DEBUG_EN      // Activa el modo de depuración.

#ifdef DEBUG_EN
#define SERIAL_BAUD 115200 // Velocidad del puerto serie.
#define DEBUG(input)         \
    {                        \
        Serial.print(input); \
        delay(1);            \
    }
#define DEBUGln(input)         \
    {                          \
        Serial.println(input); \
        delay(1);              \
    }
#else
#define DEBUG(input) ;
#define DEBUGln(input) ;
#endif

// Variables globales ########################################################

const byte totalPasswords = 5; ///< Número total de contraseñas en la memoria EEPROM externa. (Máximo 128)
const byte passwordSize = 32;  ///< Longitud de la contraseña.
const byte magic[passwordSize] =
    {"MagicTextForBasicFlashEncription"};  ///< Array para realizar el encriptado básico.
extEEPROM myEEPROM(kbits_32, 1, 32, 0x50); // Memoria de 32 Kbits (4 KBytes), 32 Byte por página.

// ###########################################################################

void setup(void)
{

    // Cambiar uno.build.f_cpu=16000000L a 8000000L en el archivo boards.txt
    // El archivo boards.txt que tiene efecto es el de la carpeta .arduino15
    noInterrupts();
    CLKPR = 0x80; // CLKPCE 1 y los demas a 0 (Procedimiento especial de escritura)
    CLKPR = 0x01; // Divisor del reloj a 2 para que funcione a 8 Mhz con un cristal de 16 Mhz.
    interrupts();

    pinMode(0, INPUT); // RX
    pinMode(LED, OUTPUT);

    // Para evitar el uso del puerto SPI cuando se quiere programar el micro.
    // No es necesario en la revisión 1.1 del hardware. (Ya está corregido el error)
    if (digitalRead(0))
    {
        delay(20);
        if (digitalRead(0))
        {
            pinMode(RFM69_RESET, OUTPUT);
            digitalWrite(RFM69_RESET, HIGH); // RFM69 desactivado.
            digitalWrite(LED, HIGH);
            delay(500);
            digitalWrite(LED, LOW);
            delay(30000); // Tiempo para poder programar el micro.
        }
    }

#ifdef DEBUG_EN
    Serial.begin(SERIAL_BAUD);
#endif

    DEBUGln("Init Setup");
    pinMode(RELAYPIN, OUTPUT);
    pinMode(RFM69_RESET, OUTPUT);

    digitalWrite(RELAYPIN, LOW);
    digitalWrite(LED, LOW);
    digitalWrite(RFM69_RESET, HIGH); // RFM69 desactivado.

    byte i2cStatus = myEEPROM.begin(extEEPROM::twiClock400kHz);
    if (i2cStatus != 0)
    {
        DEBUGln("EEPROM Problem.");
    }
    else
        DEBUGln("EEPROM Ok.");

    writePasswords(passwordSize);
    DEBUGln("Finish Setup");
}

/**
 * @brief Escribe y comprueba las contraseñas en la EEPROM.
 *
 * @param passwSize Longitud de las contraseñas.
 */
void writePasswords(byte passwSize)
{
    // Contraseñas en ASCII extendido.
    FLASH_STRING_ARRAY(passwords,
                       PSTR("U3E6]8AbMTUBxw)2w7VJU^cN_EkB];mu"),
                       PSTR("$&3>iS>N_}@(xVL4/ihbzvv3.d`BopuA"),
                       PSTR("Y)?27DEpH9D<wA#j`honHk96^$=w5W-f"),
                       PSTR("24;i.fJZf.!qW%fDe(3Z_x@+'ZfDf{6-"),
                       PSTR(">~sCgzi*3.++/.ASETJDXV[`vYnK[rf5"));

    unsigned long addr = 0;
    DEBUGln("Writing...");
    unsigned long start = millis();

    for (byte i = 0; i < totalPasswords; i++)
    {
        byte buffer[passwSize] = {0};
        byte encrypted[passwSize] = {0};
        passwords[i].copy(buffer, passwSize);
        simpleEncrypt(buffer, encrypted, passwSize);
        byte i2cStatus = myEEPROM.write(addr, encrypted, passwSize);
        addr += passwSize;

        if (i2cStatus != 0)
        {
            DEBUGln("Write error.");
            if (i2cStatus == EEPROM_ADDR_ERR)
                DEBUGln("Bad address.");
        }
    }
    unsigned long lapse = millis() - start;
    DEBUGln("Write time(ms): " + lapse);

    // Comprueba las contraseña guardadas.
    addr = 0;
    bool check_fail = false;
    DEBUGln("Reading...");
    start = millis();

    for (byte i = 0; i < totalPasswords; i++)
    {
        byte buffer[passwSize] = {0};
        byte decrypted[passwSize] = {0};
        byte i2cStatus = myEEPROM.read(addr, buffer, passwSize);
        simpleEncrypt(buffer, decrypted, passwSize);
        addr += passwSize;

        if (i2cStatus != 0)
        {
            DEBUGln("Read error.");
            if (i2cStatus == EEPROM_ADDR_ERR)
                DEBUGln("Bad address.");
        }
        else
        {
            for (byte j = 0; j < passwSize; j++)
            {
                if ((byte)passwords[i][j] != decrypted[j])
                {
                    check_fail = true;
                    DEBUGln("Pass:");
                    DEBUGln(i);
                    DEBUGln("Index:");
                    DEBUGln(j);
                    DEBUG((byte)passwords[i][j]);
                    DEBUG(" <> ");
                    DEBUGln(decrypted[j]);
                    break;
                }
            }
        }
    }

    lapse = millis() - start;
    DEBUGln("Read and check time(ms): " + lapse);

    if (check_fail)
    {
        DEBUGln("Password/s mismatch.");
        digitalWrite(LED, HIGH);
    }
    else
    {
        digitalWrite(LED, HIGH);
        delay(250);
        digitalWrite(LED, LOW);
        delay(250);
        digitalWrite(LED, HIGH);
        delay(250);
        digitalWrite(LED, LOW);
    }
}

/**
 * @brief Encripta/desencripta las constraseñas de la EEPROM mediante un simple XOR
 *
 * @param rawPassword Array de bytes de la contraseña a encriptar/desencriptar.
 * @param encryptPassword Array de bytes donde guardar la constraseña encriptada/desencriptada.
 * @param passSize Longitud de la contraseña.
 */
void simpleEncrypt(const byte *const rawPassword, byte *const encryptPassword,
                   byte passSize)
{
    for (byte i = 0; i < passSize; i++)
    {
        encryptPassword[i] = rawPassword[i] ^ magic[i];
    }
}

void loop()
{
}