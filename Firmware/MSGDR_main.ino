/*
 * File: MSGDR_main.ino
 * Project: MSGarageDoorRemote
 * Created Date: 02/09/2018 9:28:19 am
 * Author: JGReyes at <josegpuntoreyes@gmail.com>
 * -----
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any
 * later version.
 *
 *      This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 *      You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http: //www.gnu.org/licenses/>.
 * -----
 * Last Modified: 04/10/2018 3:32:36 pm
 * Modified By: JGReyes at josegpuntoreyes@gmail.com
 * -----
 * Copyright (c) 2018 JGReyes
 * -----
 * HISTORY:
 * Date      	By	JGReyes
 * ----------	---	---------------------------------------------------------
 */

// Versión para el mando.
// Fusibles a la hora de grabar el micro. lfuse: 0xFF hfuse: 0xDF efuse: 0xFE
// Lock bits: 0xE8. Para proteger contra lectura externa la memoria flash.

#include <RFM69.h>     //https://github.com/lowpowerlab/RFM69
#include <SPI.h>       //incluido con Arduino IDE
#include <Wire.h>      //incluido con Arduino IDE
#include <extEEPROM.h> //https://github.com/JChristensen/extEEPROM
#include <avr/sleep.h> //incluido con Arduino IDE

#define GATEWAYID 10            // Identificacor de la unidad de la puerta.
#define NODEID 01               // Identificador del nodo (mando).
#define NETWORKID 135           // Identificador de la red.
#define FREQUENCY RF69_868MHZ   // Frecuencia del módulo RFM69.
#define ENCRYPTKEY "EncryptKey" // Contraseña para el encriptado (16 Máximo).

#define RELAYPIN 6         // Pin donde está conectado el relé.
#define RELAY_PULSE_MS 300 // Duración del pulso en el relé.

#define LED 7

#define RFM69_SELECT_SIGNAL 5 // Pin de selección para SPI.
#define RFM69_RESET 4         // Pin de reset para poder deshabilitar el RFM69.
#define RFM69_DIO0 3          // Pin de interrupción para aviso de envio /recepción de datos.

//#define DEBUG_EN // Activa el modo de depuración.

#ifdef DEBUG_EN
#define SERIAL_BAUD 115200 // Velocidad del puerto serie.
#define DEBUG(input)         \
    {                        \
        Serial.print(input); \
        delay(1);            \
    }
#define DEBUGln(input)         \
    {                          \
        Serial.println(input); \
        delay(1);              \
    }
#else
#define DEBUG(input) ;
#define DEBUGln(input) ;
#endif

// Declaración previa para que reconozca el valor por defecto times.
void blink(unsigned int delay_time_ms, byte times = 1);

// Variables globales ########################################################

// Objeto que representa el módulo RFM69.
// Los parámetros son: pin de select para el SPI,
//                     pin de interrupción DIO0,
//                     indica si es un RFM69HW/HCW.
RFM69 radio(RFM69_SELECT_SIGNAL, RFM69_DIO0, true);
extEEPROM myEEPROM(kbits_32, 1, 32, 0x50); // Memoria de 32 Kbits (4 KBytes), 32 Byte por página.

unsigned long start_blink = 0; // Tiempo en el que se enciende el led.
unsigned long start_relay = 0; // Tiempo en el que se activa el relé.
unsigned int led_delay_ms = 0; // Tiempo que se quiere tener encendido el led.
byte led_times = 0;            // Número de veces que tiene que parpadear el led.
const byte passwordSize = 32;  ///< Longitud de la contraseña.
const byte magic[passwordSize] =
    {"MagicTextForBasicFlashEncription"}; ///< Array para realizar el encriptado básico.
const byte totalPasswords = 5;            ///< Número total de contraseñas en la memoria EEPROM externa. (Máximo 128)
bool transmitOne = false;                 // Para mandar la petición solo una vez.
bool go_sleep = false;                    // Para mandar a poner el micro en ahorro de energía.
// ###########################################################################

void setup(void)
{
    // Cambiar uno.build.f_cpu=16000000L a 8000000L en el archivo boards.txt
    // El archivo boards.txt que tiene efecto es el de la carpeta .arduino15
    noInterrupts();
    CLKPR = 0x80; // CLKPCE 1 y los demas a 0 (Procedimiento especial de escritura)
    CLKPR = 0x01; // Divisor del reloj a 2 para que funcione a 8 Mhz con un cristal de 16 Mhz.
    interrupts();

    pinMode(0, INPUT); // RX
    pinMode(LED, OUTPUT);

    // Para evitar el uso del puerto SPI cuando se quiere programar el micro.
    // No es necesario en la revisión 1.1 del hardware. (Ya está corregido el error)
    if (digitalRead(0))
    {
        delay(20);
        if (digitalRead(0))
        {
            pinMode(RFM69_RESET, OUTPUT);
            digitalWrite(RFM69_RESET, HIGH); // RFM69 desactivado.
            digitalWrite(LED, HIGH);
            delay(500);
            digitalWrite(LED, LOW);
            delay(30000); // Tiempo para poder programar el micro.
        }
    }

#ifdef DEBUG_EN
    Serial.begin(SERIAL_BAUD);
#endif

    DEBUGln("Init Setup");
    pinMode(RELAYPIN, OUTPUT);
    pinMode(RFM69_RESET, INPUT);

    digitalWrite(RELAYPIN, LOW);
    digitalWrite(LED, LOW);
    delay(10); // Espera a que termine la secuencia POR del RFM69.

    radio.initialize(FREQUENCY, NODEID, NETWORKID);
    radio.encrypt(ENCRYPTKEY);
    radio.setHighPower(true);
    radio.setPowerLevel(0); // 5dBm

    byte i2cStatus = myEEPROM.begin(extEEPROM::twiClock400kHz);
    if (i2cStatus != 0)
    {
        DEBUGln("EEPROM Problem.");
    }
    else
        DEBUGln("EEPROM Ok.");

    DEBUGln("Finish Setup");
}

void loop()
{
    static unsigned long start = millis();
    static byte buffer[3] = {0};

    check_blink();

    if (!transmitOne)
    {
        digitalWrite(LED, HIGH);
        // Manda la petición de apertura/paro/cierre de la puerta.
        buffer[0] = 0xF0;
        buffer[1] = 0xDF;
        DEBUGln("Sending request.");
        if (radio.sendWithRetry(GATEWAYID, buffer, 2))
            transmitOne = true;
        else
        {
            DEBUGln("Send fail");
            delay(50); // Espera 50 ms para volver a reintentar la transmision.
        }
    }

    // Comprueba si ha recibido paquetes de la unidad de la puerta.
    if (radio.receiveDone())
    {
        DEBUG('[');
        DEBUG(radio.SENDERID);
        DEBUG("] ");
#ifdef DEBUG_EN
        for (byte i = 0; i < radio.DATALEN; i++)
            DEBUG((char)radio.DATA[i]);
#endif
        if (radio.DATALEN == 3) // Posible petición de contraseña.
        {
            for (byte i = 0; i < 3; i++)
                buffer[i] = radio.DATA[i];

            if (buffer[0] == 0xF0 && buffer[2] == 0xF0) // Petición de contraseña.
            {
                delay(3);
                checkACK();
                byte numPassword = buffer[1]; // Número de la contraseña requerida.
                DEBUGln("Sending password...");
                sendPassword(numPassword); // Manda la contraseña requerida.
            }
        }

        if (radio.DATALEN == 2) // Posible señal de apertura correcta.
        {
            for (byte i = 0; i < 2; i++)
                buffer[i] = radio.DATA[i];

            if (buffer[0] == 0xF0 && buffer[1] == 0x5F) // Apertura correcta.
            {
                delay(3);
                checkACK();
                blink(250, 2);
                DEBUGln("RFM69 in sleep state.");
                go_sleep = true; // Pone a dormir al micro.
            }
        }
    }
}

/**
 * @brief Comprueba si se ha pedido confirmación de recepción y la manda en
 * caso afirmativo.
 */
void checkACK()
{
    if (radio.ACKRequested())
    {
        radio.sendACK();
        DEBUGln("ACK sent.");
    }
}

/**
 * @brief Manda la contraseña a la unidad del motor.
 *
 * @param numPassword Número de contraseña a mandar.
 */
void sendPassword(byte numPassword)
{
    unsigned long addr = numPassword * passwordSize;
    DEBUG("Address: ")
    DEBUGln(addr);
    byte buffer[passwordSize] = {0};
    byte decrypted[passwordSize] = {0};

    byte i2cStatus = myEEPROM.read(addr, buffer, passwordSize);
    if (i2cStatus == 0)
    {
        simpleEncrypt(buffer, decrypted, passwordSize);
#ifdef DEBUG_EN
        for (byte i = 0; i < passwordSize; i++)
            DEBUG((char)decrypted[i]);
#endif
        radio.sendWithRetry(GATEWAYID, decrypted, passwordSize);
    }
    else
    {
        DEBUGln("EEPROM read error");
        DEBUGln(i2cStatus)
    }
}

/**
 * @brief Hace parpadear el led.
 *
 * @param delay_time_ms Tiempo en ms que está encendido y apagado durante un parpadeo.
 * @param times Número de parpadeos que se quieren realizar.
 */
void blink(unsigned int delay_time_ms, byte times)
{
    led_delay_ms = delay_time_ms;
    led_times = times;
    start_blink = millis();
    digitalWrite(LED, HIGH);
}

/**
 * @brief Comprueba el estado del led y el número de parpadeos restantes
 * evitando bloquear el micro.
 */
void check_blink()
{
    if (start_blink > 0)
    {
        if ((millis() - start_blink) > led_delay_ms)
        {
            if (digitalRead(LED) == HIGH)
            {
                digitalWrite(LED, LOW);
                led_times -= 1;
            }
            else
                digitalWrite(LED, HIGH);

            if (led_times == 0)
                start_blink = 0;
            else
                start_blink = millis();
        }
    }
}

/**
 * @brief Comprueba si se quiere poner en modo de ahorro al micro y lo hace
 * una vez terminada la secuencia de parpadeos del led.
 */
void check_sleep()
{
    if (go_sleep)
    {
        if (start_blink == 0)
        {
            go_sleep = false;
            radio.sleep(); // Pone a dormir el módulo inalámbrico.
            noInterrupts();
            set_sleep_mode(SLEEP_MODE_PWR_DOWN);
            sleep_enable();
            interrupts();
            sleep_cpu();
        }
    }
}

/**
 * @brief Encripta/desencripta las constraseñas de la EEPROM mediante un simple XOR
 *
 * @param rawPassword Array de bytes de la contraseña a encriptar/desencriptar.
 * @param encryptPassword Array de bytes donde guardar la constraseña encriptada/desencriptada.
 * @param passSize Longitud de la contraseña.
 */
void simpleEncrypt(const byte *const rawPassword, byte *const encryptPassword,
                   byte passSize)
{
    for (byte i = 0; i < passSize; i++)
    {
        encryptPassword[i] = rawPassword[i] ^ magic[i];
    }
}