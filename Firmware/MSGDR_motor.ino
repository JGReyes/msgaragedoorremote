/*
 * File: MSGDR_motor.ino
 * Project: MSGarageDoorRemote
 * Created Date: 18/09/2018 6:17:31 pm
 * Author: JGReyes at <josegpuntoreyes@gmail.com>
 * -----
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any
 * later version.
 *
 *      This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 *      You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http: //www.gnu.org/licenses/>.
 * -----
 * Last Modified: 04/10/2018 3:22:08 pm
 * Modified By: JGReyes at josegpuntoreyes@gmail.com
 * -----
 * Copyright (c) 2018 JGReyes
 * -----
 * HISTORY:
 * Date      	By	JGReyes
 * ----------	---	---------------------------------------------------------
 */

// Versión para la placa conectada al motor.
// Fusibles a la hora de grabar el micro. lfuse: 0xFF hfuse: 0xDF efuse: 0xFE
// Lock bits: 0xE8. Para Proteger contra lectura la flash.

#include <RFM69.h>     //https://github.com/lowpowerlab/RFM69
#include <SPI.h>       //incluido con Arduino IDE
#include <Wire.h>      //incluido con Arduino IDE
#include <extEEPROM.h> //https://github.com/JChristensen/extEEPROM
#include <avr/wdt.h>   //incluido con Arduino IDE
#include <avr/sleep.h>
#include <avr/power.h>

#define NODEID 10               // Identificador del nodo (Unidad del motor).
#define NETWORKID 135           // Identificador de la red.
#define FREQUENCY RF69_868MHZ   // Frecuencia del módulo RFM69.
#define ENCRYPTKEY "EncryptKey" // Contraseña para el encriptado (16 Máximo).

#define RELAYPIN 6         // Pin donde está conectado el relé.
#define RELAY_PULSE_MS 300 // Duración del pulso en el relé.

#define LED 7

#define RFM69_SELECT_SIGNAL 5 // Pin de selección para SPI.
#define RFM69_RESET 4         // Pin de reset para poder deshabilitar el RFM69.
#define RFM69_DIO0 3          // Pin de interrupción para aviso de envio /recepción de datos.

//#define DEBUG_EN // Activa el modo de depuración.

#ifdef DEBUG_EN
#define SERIAL_BAUD 115200 // Velocidad del puerto serie.
#define DEBUG(input)         \
    {                        \
        Serial.print(input); \
        delay(1);            \
    }
#define DEBUGln(input)         \
    {                          \
        Serial.println(input); \
        delay(1);              \
    }
#else
#define DEBUG(input) ;
#define DEBUGln(input) ;
#endif

// Declaración previa para que reconozca el valor por defecto 'times'.
void blink(unsigned int delay_time_ms, byte times = 1);

// Variables globales ########################################################

// Objeto que representa el módulo RFM69.
// Los parámetros son: pin de select para el SPI,
//                     pin de interrupción DIO0,
//                     indica si es un RFM69HW/HCW.
RFM69 radio(RFM69_SELECT_SIGNAL, RFM69_DIO0, true);
extEEPROM myEEPROM(kbits_32, 1, 32, 0x50); // Memoria de 32 Kbits (4 KBytes), 32 Byte por página.

unsigned long start_blink = 0; // Tiempo en el que se enciende el led.
unsigned long start_relay = 0; // Tiempo en el que se activa el relé.
unsigned int led_delay_ms = 0; // Tiempo que se quiere tener encendido el led.
byte led_times = 0;            // Número de veces que tiene que parpadear el led.
const byte passwordSize = 32;  ///< Longitud de la contraseña.
const byte magic[passwordSize] =
    {"MagicTextForBasicFlashEncription"}; ///< Array para realizar el encriptado básico.
const byte totalPasswords = 5;            ///< Número total de contraseñas en la memoria EEPROM externa. (Máximo 128)
byte numPassword = 0;                     // Número de la contraseña requerida.

// ###########################################################################

void setup(void)
{
    // Cambiar uno.build.f_cpu=16000000L a 8000000L en el archivo boards.txt
    // El archivo boards.txt que tiene efecto es el de la carpeta .arduino15
    noInterrupts();
    CLKPR = 0x80; // CLKPCE 1 y los demas a 0 (Procedimiento especial de escritura)
    CLKPR = 0x01; // Divisor del reloj a 2 para que funcione a 8 Mhz con un cristal de 16 Mhz.
    interrupts();

    pinMode(0, INPUT); // RX
    pinMode(LED, OUTPUT);

    // Para evitar el uso del puerto SPI cuando se quiere programar el micro.
    // No es necesario en la revisión 1.1 del hardware. (Ya está corregido el error)
    if (digitalRead(0))
    {
        delay(20);
        if (digitalRead(0))
        {
            pinMode(RFM69_RESET, OUTPUT);
            digitalWrite(RFM69_RESET, HIGH); // RFM69 desactivado.
            digitalWrite(LED, HIGH);
            delay(500);
            digitalWrite(LED, LOW);
            delay(30000); // Tiempo para poder programar el micro.
        }
    }

#ifdef DEBUG_EN
    Serial.begin(SERIAL_BAUD);
#endif

    DEBUGln("Init Setup");
    pinMode(RELAYPIN, OUTPUT);
    pinMode(RFM69_RESET, INPUT);

    digitalWrite(RELAYPIN, LOW);
    digitalWrite(LED, LOW);
    delay(10); // Espera a que termine la secuencia POR del RFM69.

    radio.initialize(FREQUENCY, NODEID, NETWORKID);
    radio.encrypt(ENCRYPTKEY);
    radio.setHighPower(true);
    radio.setPowerLevel(0); // 5dBm

    byte i2cStatus = myEEPROM.begin(extEEPROM::twiClock400kHz);
    if (i2cStatus != 0)
    {
        DEBUGln("EEPROM Problem.");
    }
    else
        DEBUGln("EEPROM Ok.");

    // Configuración del watchdog.
    noInterrupts();
    wdt_reset();
    MCUSR &= ~(1 << WDRF);                           // Pone a 0 el bit WDRF, que indica si ha sudidido un reset por watchdog.
    WDTCSR |= (1 << WDCE) | (1 << WDE);              // Secuencia para poder realizar cambios en el watchdog.
    WDTCSR = (1 << WDE) | (1 << WDP3) | (1 << WDP0); // System Reset Mode y 8 segundos de intervalo.
    interrupts();

    DEBUGln("Finish Setup");

    // Para ver cuando se incicia o reinicia (a causa del watchdog) el micro.
    digitalWrite(LED, HIGH);
    delay(3000);
    digitalWrite(LED, LOW);
}

EMPTY_INTERRUPT(WDT_vect);

void loop()
{
    static unsigned long start = millis();
    static byte buffer[passwordSize] = {0};
    static bool doorActionRequest = false; // Indica si se ha mandado una petición a la puerta.
    static bool readyForSleep = false;     // Indica si se quiere poner a domir el micro.
    static unsigned long startRequest = 0; // Tiempo en que se solicitó una apertura/cierre de la puerta.
    static unsigned long startWakeUp = 0;  // Tiempo activo del micro.
    // Hace parpadear el led cada 5 segundos cuando está a la espera.
    // con el modo de sleep funcionando se ejecuta cada 10 segudnos aprox.
    if ((millis() - start) > 5000)
    {
        blink(500);
        start = millis();
    }

    check_relay();
    check_blink();
    wdt_reset(); // Reinicia el watchdog.

    // Comprueba si ha recibido paquetes de la unidad de la puerta.
    if (radio.receiveDone())
    {
        DEBUG('[');
        DEBUG(radio.SENDERID);
        DEBUG("] ");
#ifdef DEBUG_EN
        for (byte i = 0; i < radio.DATALEN; i++)
            DEBUG((char)radio.DATA[i]);
#endif

        if (radio.DATALEN == 2) // Posible señal de petición de apertura/cierre.
        {
            for (byte i = 0; i < 2; i++)
                buffer[i] = radio.DATA[i];

            if (buffer[0] == 0xF0 && buffer[1] == 0xDF)
            {
                delay(3);
                checkACK();
                DEBUGln("Open/close request received.");
                numPassword++;
                if (numPassword > totalPasswords - 1)
                    numPassword = 0;
                buffer[0] = 0xF0;
                buffer[1] = numPassword;
                buffer[2] = 0xF0;
                // Envio el número de contraseña requerida.
                DEBUGln("Sending password number.");
                radio.sendWithRetry(radio.SENDERID, buffer, 3);
                doorActionRequest = true;
                startRequest = millis();
            }
        }

        if (doorActionRequest && radio.DATALEN == passwordSize) // Contraseña.
        {
            byte sender = radio.SENDERID;
            delay(3);
            checkACK();
            DEBUGln("Password received.");
            doorActionRequest = false; // Para no entrar de nuevo sin una petición previa.
            startRequest = 0;

            for (byte i = 0; i < passwordSize; i++)
                buffer[i] = radio.DATA[i];

            if (checkPassword(buffer))
            {
                DEBUGln("Password OK.");
                blink(250, 2);
                pulseRelay(); // Abrimos/Cerramos la puerta.
                buffer[0] = 0xF0;
                buffer[1] = 0x5F;
                // Manda la confirmación de la apertura/cierre.
                DEBUG("Sending request OK to ");
                DEBUGln(sender);
                radio.sendWithRetry(sender, buffer, 2);
            }
        }
    }
    else
    { // Si no hay ninguna petición en marcha y ha pasado medio segundo
        // se pone el micro en modo sleep.
        if (startRequest == 0 && (millis() - startWakeUp) > 500)
            readyForSleep = true;
    }

    // Si pasan dos segundos desde que se pide la apertura/cierre y no se recibe la contraseña
    // se anula la petición.
    if (startRequest > 0 && (millis() - startRequest) > 2000)
    {
        startRequest = 0;
        doorActionRequest = false;
    }

    if (readyForSleep && start_blink == 0 && start_relay == 0)
    {
        readyForSleep = false;
        // Ponemos el procesador y la radio en modo sleep para ahorrar energía.
        radio.sleep();
        power_timer1_disable();
        power_timer2_disable();
        power_usart0_disable();
        power_adc_disable();

        noInterrupts();
        wdt_reset();
        MCUSR &= ~(1 << WDRF);                                         // Pone a 0 el bit WDRF, que indica si ha sudidido un reset por watchdog.
        WDTCSR |= (1 << WDCE) | (1 << WDE);                            // Secuencia para poder realizar cambios en el watchdog.
        WDTCSR = (1 << WDE) | (1 << WDIE) | (1 << WDP2) | (1 << WDP0); // Interrupt and System Reset Mode y 500ms de intervalo.
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable();
        interrupts();
        sleep_cpu();

        // Punto de ejecución después de despertarse el micro.
        sleep_disable();
        WDTCSR |= (1 << WDIE);  // Reactiva la interrupción por watchdog.
        radio.receiveDone();    // Reactiva la radio.
        startWakeUp = millis(); // Guarda el tiempo en el que se activa el micro.
    }
}

/**
 * @brief Comprueba si se ha pedido confirmación de recepción y la manda en
 * caso afirmativo.
 */
void checkACK()
{
    if (radio.ACKRequested())
    {
        radio.sendACK();
        DEBUGln("ACK sent.");
    }
}

/**
 * @brief Comprueba la contraseña recibida.
 *
 * @param receivedPassword Array de bytes con la contraseña recibida.
 * @return true Si la contraseña es válida.
 * @return false En caso contrario o error de lectura de EEPROM.
 */
bool checkPassword(const byte *const receivedPassword)
{
    DEBUGln("Checking password.");
    unsigned long addr = numPassword * passwordSize;
    DEBUGln(addr);
    byte reqPassword[passwordSize] = {0};
    byte decrypted[passwordSize] = {0};

    byte i2cStatus = myEEPROM.read(addr, reqPassword, passwordSize);
    if (i2cStatus == 0)
    {
        simpleEncrypt(reqPassword, decrypted, passwordSize);
#ifdef DEBUG_EN
        for (byte i = 0; i < passwordSize; i++)
            DEBUG((char)decrypted[i]);
#endif
        for (byte i = 0; i < passwordSize; i++)
        {
            if (receivedPassword[i] != decrypted[i])
                return false;
        }
        return true;
    }
    return false;
}

/**
 * @brief Encripta/desencripta las constraseñas de la EEPROM mediante un simple XOR
 *
 * @param rawPassword Array de bytes de la contraseña a encriptar/desencriptar.
 * @param encryptPassword Array de bytes donde guardar la constraseña encriptada/desencriptada.
 * @param passSize Longitud de la contraseña.
 */
void simpleEncrypt(const byte *const rawPassword, byte *const encryptPassword,
                   byte passSize)
{
    for (byte i = 0; i < passSize; i++)
    {
        encryptPassword[i] = rawPassword[i] ^ magic[i];
    }
}

/**
 * @brief Activa el relé para abrir la puerta.
 *
 */
void pulseRelay()
{
    start_relay = millis();
    digitalWrite(RELAYPIN, HIGH);
}

/**
 * @brief Comprueba el tiempo que está activado el relé para apagarlo cuando
 * llegue al tiempo especificado en la variable global RELAY_PULSE_MS.
 */
void check_relay()
{
    if (start_relay > 0)
    {
        if ((millis() - start_relay) > RELAY_PULSE_MS)
        {
            digitalWrite(RELAYPIN, LOW);
            start_relay = 0;
        }
    }
}

/**
 * @brief Hace parpadear el led.
 *
 * @param delay_time_ms Tiempo en ms que está encendido y apagado durante un parpadeo.
 * @param times Número de parpadeos que se quieren realizar.
 */
void blink(unsigned int delay_time_ms, byte times)
{
    led_delay_ms = delay_time_ms;
    led_times = times;
    start_blink = millis();
    digitalWrite(LED, HIGH);
}

/**
 * @brief Comprueba el estado del led y el número de parpadeos restantes
 * evitando bloquear el micro.
 */
void check_blink()
{
    if (start_blink > 0)
    {
        if ((millis() - start_blink) > led_delay_ms)
        {
            if (digitalRead(LED) == HIGH)
            {
                digitalWrite(LED, LOW);
                led_times -= 1;
            }
            else
                digitalWrite(LED, HIGH);

            if (led_times == 0)
                start_blink = 0;
            else
                start_blink = millis();
        }
    }
}